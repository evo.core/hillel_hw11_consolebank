package com.consolebank;

import com.consolebank.accounts.*;
import com.consolebank.consolehost.ConsoleDataProvider;
import com.consolebank.consolehost.DataProvider;

public class Main {

    public static void main(String[] args) {

        BankAccount currentClientAccount;
        DataProvider consoleDataProvider = new ConsoleDataProvider();

        consoleDataProvider.createTestData();
        currentClientAccount = consoleDataProvider.insertCard();
        consoleDataProvider.getBalance(currentClientAccount);
        consoleDataProvider.putDeposit(currentClientAccount);
        consoleDataProvider.withdrawalMoney(currentClientAccount);

    }
}
