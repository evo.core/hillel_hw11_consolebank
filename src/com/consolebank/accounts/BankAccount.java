package com.consolebank.accounts;

import java.util.Date;

import com.consolebank.exceptions.BalanceMinusException;
import com.consolebank.enums.CurrencyType;
import com.consolebank.exceptions.IncorrectPinException;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;

public class BankAccount {
    private long accountNumber;
    private long cardNumber;
    private CurrencyType currency;
    private double balance;
    private Date date;
    private String userName;
    private int pin;


    public BankAccount(String userName, long accountNumber, long cardNumber, CurrencyType currency, double balance, int pin) {
        this.accountNumber = accountNumber;
        this.cardNumber = cardNumber;
        this.balance = balance;
        this.currency = currency;
        this.date = new Date();
        this.userName = userName;
        this.pin = pin;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void getBalance(int pin) throws IncorrectPinException {

        if (this.pin == pin) {
            System.out.println("Your balance: " + balance);
        } else {
            throw new IncorrectPinException(pin);

        }

    }

    public Date getDate() {
        return date;
    }

    public String getUserName() {
        return userName;
    }

    public boolean getMoney(double sum, int pin) throws BalanceMinusException, IncorrectPinException {

        int checkOk;

        if (this.pin == pin) {
            if (balance < sum)
                throw new BalanceMinusException(balance, sum);
            balance -= sum;
            System.out.println("Your balance: " + balance);
            return true;
        } else {
            throw new IncorrectPinException(pin);
        }
    }

    public void putMoney(double sum) {
        balance += sum;
    }


    @Override
    public String toString() {
        return "Name =" + "\t" + userName + "\n" +
                "Account number =" + "\t" + accountNumber + "\n" +
                "Card number =" + "\t" + cardNumber + "\n" +
                "Balance =" + "\t" + balance + "\n" +
                "Currency = " + currency + "\n" +
                "date = " + date();
    }

}