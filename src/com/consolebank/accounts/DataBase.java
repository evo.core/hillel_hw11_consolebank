package com.consolebank.accounts;

import com.consolebank.exceptions.NoBankAccountException;

import java.util.ArrayList;

public class DataBase {

    private ArrayList<BankAccount> data = new ArrayList<BankAccount>();

    public BankAccount searchCard(long cardNumber) throws NoBankAccountException {

        BankAccount clientAccount = null;
        boolean searchOk = false;

        for (int i = 0; i < data.size(); i++) {
            if (cardNumber == data.get(i).getCardNumber()) {
                clientAccount = data.get(i);
                searchOk = true;
            }
        }
        if (!searchOk) {
            throw new NoBankAccountException(cardNumber);
        }
        return clientAccount;
    }

    public void addAccount(BankAccount newBankAccount) {

        data.add(newBankAccount);

    }

    public void addMoney(BankAccount clientAccount, double sum) {

        long cardNumber = clientAccount.getCardNumber();

        boolean searchOk = false;

        for (int i = 0; i < data.size(); i++) {
            if (cardNumber == data.get(i).getCardNumber()) {
                data.get(i).putMoney(sum);
                searchOk = true;
            }
        }
        if (searchOk) {
            System.out.println("You amount deposit " + sum + " successfully added.");
        }
    }

    public int getCardQuantity() {
        return data.size()-1;
    }


}
