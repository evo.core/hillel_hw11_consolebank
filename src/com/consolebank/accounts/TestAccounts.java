package com.consolebank.accounts;

import com.consolebank.enums.CardType;
import com.consolebank.enums.CurrencyType;
import com.consolebank.accounts.CreateAccount;
import com.consolebank.purse.Card;

import java.util.ArrayList;

public class TestAccounts {

    private ArrayList<Card> cardStorage = new ArrayList<Card>();
    private CreateAccount createAccount = new CreateAccount();

    public ArrayList<Card> getCardStorage() {
        return cardStorage;
    }

    public BankAccount createTestAccountA() {
        createTestCard(5355015801279292L, "John Smith", CardType.MASTERCARD);
        return createAccount.createNewAccount("John Smith", 5355015801279292L, 5355015801279292L, CurrencyType.USD, 90000, 1111);

    }

    public BankAccount createTestAccountB() {
        createTestCard(5300028822445599L, "Derek Vincent Smith", CardType.MASTERCARD);
        return createAccount.createNewAccount("Derek Vincent Smith", 5300028822445599L, 5300028822445599L, CurrencyType.USD, 9922000, 1234);

    }

    public BankAccount createTestAccountC() {
        createTestCard(5300028822884488L, "Василий Иванов", CardType.VISA);
        return createAccount.createNewAccount("Василий Иванов", 5300028822884488L, 5300028822884488L, CurrencyType.UAH, 32000, 3200);
    }

    public void createTestCard(long cardNumber, String holderName, CardType cardType) {
        Card card = new Card(holderName, cardNumber, cardType);
        cardStorage.add(card);
    }

    public int getTestCardsQuantity(){
        return cardStorage.size();
    }


}
