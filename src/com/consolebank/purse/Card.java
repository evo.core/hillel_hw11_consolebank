package com.consolebank.purse;

import com.consolebank.enums.CardType;

public class Card {


    private String holderName;
    private long cardNumber;
    private CardType cardType;

    public Card(String holderName, long cardNumber, CardType cardType) {
        this.holderName = holderName;
        this.cardNumber = cardNumber;
        this.cardType = cardType;
    }


    public String getHolderName() {
        return holderName;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public CardType getCardType() {
        return cardType;
    }

    @Override
    public String toString () {
        return "Holder Name =" + "\t" + holderName + "\n" +
                "Card number =" + "\t" + cardNumber + "\n" +
                cardType;
    }

}
