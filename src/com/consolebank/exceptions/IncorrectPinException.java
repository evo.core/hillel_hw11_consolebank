package com.consolebank.exceptions;

public class IncorrectPinException extends Exception {
    public IncorrectPinException (int pin){
        super("You entered an incorrect pin" + pin);
    }

}
