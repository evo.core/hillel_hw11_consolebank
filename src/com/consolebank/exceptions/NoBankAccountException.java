package com.consolebank.exceptions;

public class NoBankAccountException extends Exception {

    public NoBankAccountException(long cardNumber) {
        super("Wrong card : " + cardNumber);
    }

}


