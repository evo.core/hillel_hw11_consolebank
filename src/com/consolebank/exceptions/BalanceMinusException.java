package com.consolebank.exceptions;

public class BalanceMinusException extends Exception {

    public BalanceMinusException(double balance, double sum) {
        super("Not enough money in the account" + "\n" +
                "Balance :" +balance + "\n" +
                "Sum :" + sum);
    }
}
