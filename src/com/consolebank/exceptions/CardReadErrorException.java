package com.consolebank.exceptions;

public class CardReadErrorException extends Exception  {

    public CardReadErrorException () {
        super("Card broken or not existing");
    }
}
