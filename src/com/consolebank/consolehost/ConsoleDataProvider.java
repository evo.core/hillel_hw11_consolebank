package com.consolebank.consolehost;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.consolebank.accounts.BankAccount;
import com.consolebank.accounts.TestAccounts;
import com.consolebank.enums.CardType;
import com.consolebank.enums.CurrencyType;
import com.consolebank.exceptions.BalanceMinusException;
import com.consolebank.exceptions.CardReadErrorException;
import com.consolebank.exceptions.IncorrectPinException;

import com.consolebank.accounts.DataBase;
import com.consolebank.exceptions.NoBankAccountException;

public class ConsoleDataProvider implements DataProvider {

    private TestAccounts testData = new TestAccounts();
    private DataBase dataBase = new DataBase();


    private static Scanner scanner = new Scanner(System.in);
    private BankAccount clientAccount;

    public void createTestData() {
        dataBase.addAccount(testData.createTestAccountA());
        dataBase.addAccount(testData.createTestAccountB());
        testData.createTestCard(5100222233334444L, "Broken Card", CardType.VISA);
        dataBase.addAccount(testData.createTestAccountC());
    }


    private BankAccount insertCardSearch() throws CardReadErrorException, NoBankAccountException {

        System.out.print("Choose one of " + (testData.getTestCardsQuantity()) + " cards :");

            int card = scanner.nextInt();
            if (card - 1 < testData.getTestCardsQuantity() && card - 1 >= 0) {
                long cardNumber = testData.getCardStorage().get(card - 1).getCardNumber();
                clientAccount = dataBase.searchCard(cardNumber);
                return clientAccount;
            } else {
                throw new CardReadErrorException();
            }
    }

    public BankAccount insertCard() {
        try {
            clientAccount = insertCardSearch();
            return clientAccount;
        } catch (CardReadErrorException e) {
            System.out.println(" Card not existing ");
            insertCard();
        } catch (NoBankAccountException e) {
            System.out.println("Unknown card. ");
            insertCard();
        }
        return clientAccount;
    }


    @Override
    public void withdrawalMoney(BankAccount bankAccount) {
        double sum;
        int pin = 0;
        System.out.println("Enter pin: ");
        try {
            pin = scanner.nextInt();
            System.out.println("Enter the amount you wish to withdraw: ");
            sum = scanner.nextDouble();
            bankAccount.getMoney(sum, pin);
        } catch (InputMismatchException e) {
            System.err.println("You should provide only Number value");
            withdrawalMoney(bankAccount);
        } catch (IncorrectPinException e) {
            System.err.println("Pin incorrect");
            withdrawalMoney(bankAccount);
        } catch (BalanceMinusException e) {
            e.printStackTrace();
            withdrawalMoney(bankAccount);
        }
    }

    @Override
    public double putDeposit(BankAccount bankAccount) {
        double balance = 0;
        System.out.println("Enter the amount you want to deposit: ");
        try {
            balance = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.err.println("You should provide only Number value");
            getBalance(bankAccount);
        }
        dataBase.addMoney(bankAccount, balance);
        return balance;
    }

    @Override
    public void getBalance(BankAccount bankAccount) {
        int pin = 0;
        System.out.println("Enter pin: ");
        try {
            pin = scanner.nextInt();
            bankAccount.getBalance(pin);
        } catch (IncorrectPinException e) {
            System.out.println("Incorrect pin!");
            getBalance(bankAccount);
        }
    }


}
