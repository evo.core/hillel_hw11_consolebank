package com.consolebank.consolehost;

import com.consolebank.accounts.BankAccount;

public interface DataProvider {

        void createTestData();
        BankAccount insertCard();
        void getBalance(BankAccount bankAccount);
        double putDeposit(BankAccount bankAccount);
        void withdrawalMoney(BankAccount bankAccount);

}
